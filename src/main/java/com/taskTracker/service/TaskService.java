package com.taskTracker.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taskTracker.dao.TaskRepo;
import com.taskTracker.model.Task;

@Service
public class TaskService {
	
	@Autowired
	private TaskRepo taskRepo;
	
	public List<Task> retrieveAllTasksByEmail(String email){
		
		List<Task> tasklist = new ArrayList<>();
		
		taskRepo.findByUserEmail(email).forEach(tasklist::add);
		return tasklist;
	}
	
	
	public Task addnewTask(Task task) {
		return taskRepo.save(task);
	}
	
	public void removeTask(Long taskid) {
		//Optional<Task> task =  taskRepo.findById(taskid);
		taskRepo.deleteById(taskid);
	}
	
	public Optional<Task> retriveTaskbyId(Long id) {	
		return taskRepo.findById(id);
	}
}
