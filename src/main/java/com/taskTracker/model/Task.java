package com.taskTracker.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tasks")
public class Task {

	@Transient
	public static final String SEQUENCE_NAME = "tasks_sequence";

	@Id
	private long id;

	@NotBlank
	@Size(max = 100)
	@Indexed(unique = true)
	private String userEmail;
	
	@NotBlank
	@Size(max = 250)
	private String taskTitle;
	
	@Size(max = 1000)
	private String taskDescription;
	
	public Task() {};
	
	public Task(@NotBlank @Size(max = 100) String userEmail, @NotBlank @Size(max = 250) String taskTitle,
			@Size(max = 1000) String taskDescription) {
		
		this.userEmail = userEmail;
		this.taskTitle = taskTitle;
		this.taskDescription = taskDescription;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", userEmail=" + userEmail + ", taskTitle=" + taskTitle + ", taskDescription="
				+ taskDescription + "]";
	}
	
	

}
