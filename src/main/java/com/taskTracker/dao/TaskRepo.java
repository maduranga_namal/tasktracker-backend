package com.taskTracker.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.taskTracker.model.Task;

@Repository
public interface TaskRepo extends CrudRepository<Task, Long> {

	List<Task> findByUserEmail(String userEmail);
}
