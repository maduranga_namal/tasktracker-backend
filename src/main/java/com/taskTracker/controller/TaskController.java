package com.taskTracker.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.taskTracker.model.Task;
import com.taskTracker.service.SequenceGenerator;
import com.taskTracker.service.TaskService;

@CrossOrigin
@RestController
@RequestMapping("/tasktracker/v1")
public class TaskController {

	@Autowired
	TaskService taskService;

	@Autowired
	private SequenceGenerator SequenceGenerator;

	@GetMapping("/tasks/{email}")
	public List<Task> getAlListTasks(@PathVariable String email) {
		return taskService.retrieveAllTasksByEmail(email);
	}
	
	@GetMapping("task/{id}")
	public Optional<Task> getTask(@PathVariable(value = "id") Long taskid) {
		return taskService.retriveTaskbyId(taskid);
	}

	@PostMapping("/task")
	public Task createTask(@Valid @RequestBody Task task) {
		task.setId(SequenceGenerator.generateSequence(Task.SEQUENCE_NAME));
		return taskService.addnewTask(task);
	}

	@DeleteMapping("/task/{id}")
	public void deleteTask(@PathVariable(value = "id") Long taskId) {
		taskService.removeTask(taskId);

	}

}
